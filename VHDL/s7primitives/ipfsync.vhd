-- Primitive wrapper for Zynq Series 7
-- to be used in:

-- ADC500.vhd
-- ADC500_NO_FIFO.vhd

-- Descrpition: Differentical clock input buffer to single ended. 

-- Revision:
-- 0.01 - File created.

----------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

entity fsync is
    GENERIC(FWFT  : BOOLEAN :=TRUE);
    PORT (
        RSTN : IN STD_LOGIC;
        
        CLKI : IN STD_LOGIC;
        CLKR : IN STD_LOGIC;

        DIN : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        DOUT : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        RDY  : IN STD_LOGIC;
        DVALID : OUT STD_LOGIC
    );

end fsync;

architecture RTL of fsync is

    signal rst, dv, empty : std_logic;

    COMPONENT ffsync
    PORT (
        rst : IN STD_LOGIC;
        wr_clk : IN STD_LOGIC;
        rd_clk : IN STD_LOGIC;
        din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        wr_en : IN STD_LOGIC;
        rd_en : IN STD_LOGIC;
        dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        full : OUT STD_LOGIC;
        empty : OUT STD_LOGIC
    );
    END COMPONENT;

begin

    FIFO_BUFF : ffsync
    PORT MAP (
        rst => rst,
        wr_clk => CLKI,
        rd_clk => CLKR,
        din => DIN,
        wr_en => RDY,
        rd_en => dv,
        dout => DOUT,
        full => OPEN,
        empty => empty
    );
  rst<=not(RSTN);
  dv<=not(empty);
  DVALID<=dv;

end RTL;

architecture PRIM of fsync is

    signal rst, dv, empty : std_logic;

begin

    FIFO18E1_inst : FIFO18E1
    generic map (
        ALMOST_EMPTY_OFFSET => X"0080", -- Sets the almost empty threshold
        ALMOST_FULL_OFFSET => X"0080", -- Sets almost full threshold
        DATA_WIDTH => 36, -- Sets data width to 36
        DO_REG => 1, -- Enable output register 1 for dual clock
        EN_SYN => FALSE, -- Specifies FIFO as dual-clock (FALSE) or Synchronous (TRUE)
        FIFO_MODE => "FIFO18_36", -- Sets mode to FIFO18_36
        FIRST_WORD_FALL_THROUGH => FWFT, -- Sets the FIFO FWFT to FALSE, TRUE
        INIT => X"000000000", -- Initial values on output port
        SIM_DEVICE => "7SERIES", -- Must be set to "7SERIES" for simulation behavior
        SRVAL => X"000000000" -- Set/Reset value for output port
    )
    port map (
        -- Read Data: 32-bit (each) output: Read output data
        DO => DOUT, -- 32-bit output: Data output
        DOP => OPEN, -- 4-bit output: Parity data output
        -- Status: 1-bit (each) output: Flags and other FIFO status outputs
        ALMOSTEMPTY => OPEN, -- 1-bit output: Almost empty flag
        ALMOSTFULL => OPEN, -- 1-bit output: Almost full flag
        EMPTY => empty, -- 1-bit output: Empty flag
        FULL => OPEN, -- 1-bit output: Full flag
        RDCOUNT => OPEN, -- 12-bit output: Read count
        RDERR => OPEN, -- 1-bit output: Read error
        WRCOUNT => OPEN, -- 12-bit output: Write count
        WRERR => OPEN, -- 1-bit output: Write error
        -- Read Control Signals: 1-bit (each) input: Read clock, enable and reset input signals
        RDCLK => CLKR, -- 1-bit input: Read clock
        RDEN => dv, -- 1-bit input: Read enable
        REGCE => '1', -- 1-bit input: Clock enable
        RST => rst, -- 1-bit input: Asynchronous Reset
        RSTREG => rst, -- 1-bit input: Output register set/reset
        -- Write Control Signals: 1-bit (each) input: Write clock and enable input signals
        WRCLK => CLKI, -- 1-bit input: Write clock
        WREN => RDY, -- 1-bit input: Write enable
        -- Write Data: 32-bit (each) input: Write input data
        DI => DIN, -- 32-bit input: Data input
        DIP => "0000" -- 4-bit input: Parity input
    );

  rst<=not(RSTN);
  dv<=not(empty);
  DVALID<=dv;

end PRIM;