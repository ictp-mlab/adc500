library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
Library UNISIM;
use UNISIM.vcomponents.all;


package primitives is

    component idatabuf is
        Generic(Nbits : integer);
        Port(
        data_from_adc_DS_N : IN STD_LOGIC_VECTOR(Nbits - 1 downto 0);
        data_from_adc_DS_P : IN STD_LOGIC_VECTOR(Nbits - 1 downto 0);
        DATA    : OUT STD_LOGIC_VECTOR(Nbits - 1 downto 0)
        );
    end component;
    
    component oclkbuf is
        Port(
        clk_in : IN std_logic;
        clk_o_p : OUT std_logic;
        clk_o_n : OUT std_logic
        );
    end component;
    
    component iclkbuf is
        Port(
            clk_in_p : IN std_logic;
            clk_in_n : IN std_logic;
            clk_o : OUT std_logic
        );
    end component;
    
    component fsync is
        PORT (
            RSTN : IN STD_LOGIC;
            CLKI : IN STD_LOGIC;
            CLKR : IN STD_LOGIC;   
            DIN : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            DOUT : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            RDY  : IN STD_LOGIC;
            DVALID : OUT STD_LOGIC
        );
    end component;
 

end primitives;