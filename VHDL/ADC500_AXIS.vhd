----------------------------------------------------------------------------------
-- Company: ICTP-MLAB
-- Engineer: Luis G. Garcia
--
-- Create Date: 07/19/2018 04:14:02 PM
-- Design Name: ADC500 controller
-- Module Name: adc500_controller - Behavioral
-- Project Name: ICTP-INFN ADC500 board
-- Target Devices: Zynq, Ultrascale+
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.primitives.all;

entity adc500_axis_controller is
    generic (axidwidth : integer :=32;
             aximinburst  : integer :=255;
             IN_reg_bits : integer :=32;
	         OUT_reg_bits : integer :=32);
    port (
        m_axis_rstn : IN STD_LOGIC;
        m_axis_aclk : IN STD_LOGIC;

        maxis_cfg : IN STD_LOGIC_VECTOR(IN_reg_bits-1 downto 0); --nTKEEP and burst size configuration.
        --AXI STREAM INTERFACE
        m_axis_tvalid : OUT STD_LOGIC;
        m_axis_tready : IN STD_LOGIC;
        m_axis_tdata  : OUT STD_LOGIC_VECTOR(axidwidth -1  downto 0);
        m_axis_tkeep  : OUT STD_LOGIC_VECTOR(axidwidth/8 -1 downto 0);
        m_axis_tlast  : OUT STD_LOGIC;

        -- Raw data output
        RAW_DATA  : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
        RAW_CK   : OUT STD_LOGIC;
        RAW_TIMER : OUT STD_LOGIC_VECTOR(64 -1 DOWNTO 0);
        
        --500 Mhz Clock buffer 
        ADC500_CLK_I : IN STD_LOGIC;
        clk_to_adc_DS_P : OUT STD_LOGIC;
        clk_to_adc_DS_N : OUT STD_LOGIC;

        --250 MHz Data Clock Input
        CLK_IN1_D_clk_p : IN STD_LOGIC;
        CLK_IN1_D_clk_n : IN STD_LOGIC;


        --Data buffer
        data_from_adc_DS_N : IN STD_LOGIC_VECTOR(15 downto 0);
        data_from_adc_DS_P : IN STD_LOGIC_VECTOR(15 downto 0);

		Ctrl_reg_in  : in std_logic_vector(IN_reg_bits-1 downto 0);
        Ctrl_reg_out : out std_logic_vector(OUT_reg_bits-1 downto 0);
        
        x_from_adc_calrun_fmc: in std_logic;    --                          
        x_from_adc_or_p : in std_logic;           -- adc (differential) lvds signal  		
        x_from_adc_or_n : in std_logic;           -- adc (differential) lvds signal  		
  
        x_to_adc_cal_fmc         : out std_logic ;   --                               
        x_to_adc_caldly_nscs_fmc : out std_logic ;  --                         
        x_to_adc_fsr_ece_fmc     : out std_logic ;  --                                  
        x_to_adc_outv_slck_fmc   : out std_logic ;  --                                
        x_to_adc_outedge_ddr_sdata_fmc : out std_logic ;  --                        
        x_to_adc_dclk_rst_fmc : out std_logic ;   --                                 
        x_to_adc_pd_fmc : out std_logic ;  -- 
        x_to_adc_led_0    : out std_logic ;
        x_to_adc_led_1    : out std_logic
	);
end adc500_axis_controller;
--
architecture arch_imp of adc500_axis_controller is
    
    component adc500_controller is
        generic (IN_reg_bits : integer :=32;
                 OUT_reg_bits : integer :=32);
        port (
            RSTN : IN STD_LOGIC;
            SYSCLK : IN STD_LOGIC;
            
            DVALID : OUT STD_LOGIC;
            DOUT : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            REN  : IN STD_LOGIC; --READ ENABLE
    
    
           -- Raw data output
            RAW_DATA  : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
            RAW_CK   : OUT STD_LOGIC;
            RAW_TIMER : OUT STD_LOGIC_VECTOR(64 -1 DOWNTO 0);
    
            --500 Mhz Clock buffer 
            ADC500_CLK_I : IN STD_LOGIC;
            clk_to_adc_DS_P : OUT STD_LOGIC;
            clk_to_adc_DS_N : OUT STD_LOGIC;
    
            --250 MHz Data Clock Input
            CLK_IN1_D_clk_p : IN STD_LOGIC;
            CLK_IN1_D_clk_n : IN STD_LOGIC;
    
    
            --Data buffer
            data_from_adc_DS_N : IN STD_LOGIC_VECTOR(15 downto 0);
            data_from_adc_DS_P : IN STD_LOGIC_VECTOR(15 downto 0);
    
            Ctrl_reg_in  : in std_logic_vector(IN_reg_bits-1 downto 0);
            Ctrl_reg_out : out std_logic_vector(OUT_reg_bits-1 downto 0);
            
            x_from_adc_calrun_fmc: in std_logic;    --                          
            x_from_adc_or_p : in std_logic;           -- adc (differential) lvds signal  		
            x_from_adc_or_n : in std_logic;           -- adc (differential) lvds signal  		
      
            x_to_adc_cal_fmc         : out std_logic ;   --                               
            x_to_adc_caldly_nscs_fmc : out std_logic ;  --                         
            x_to_adc_fsr_ece_fmc     : out std_logic ;  --                                  
            x_to_adc_outv_slck_fmc   : out std_logic ;  --                                
            x_to_adc_outedge_ddr_sdata_fmc : out std_logic ;  --                        
            x_to_adc_dclk_rst_fmc : out std_logic ;   --                                 
            x_to_adc_pd_fmc : out std_logic ;  -- 
            x_to_adc_led_0    : out std_logic ;
            x_to_adc_led_1    : out std_logic
        );
    end component adc500_controller;
    
    signal cmax : unsigned(IN_reg_bits-4 downto 0);
    signal burst : unsigned(IN_reg_bits -4 downto 0);
    signal counter : unsigned(IN_reg_bits -4 downto 0);

begin
    
        adc500: adc500_controller
            generic map(IN_reg_bits  =>32,
                        OUT_reg_bits =>32)
            port map(
                RSTN                            =>m_axis_rstn,
                SYSCLK                          =>m_axis_aclk,
                DVALID                          =>m_axis_tvalid,
                DOUT                            =>m_axis_tdata,
                REN                             =>m_axis_tready,
                RAW_DATA                        =>RAW_DATA,
                RAW_CK                          =>RAW_CK,
                RAW_TIMER                       =>RAW_TIMER,
                ADC500_CLK_I                    =>ADC500_CLK_I,
                clk_to_adc_DS_P                 =>clk_to_adc_DS_P,
                clk_to_adc_DS_N                 =>clk_to_adc_DS_N,
                CLK_IN1_D_clk_p                 =>CLK_IN1_D_clk_p,
                CLK_IN1_D_clk_n                 =>CLK_IN1_D_clk_n,
                data_from_adc_DS_N              =>data_from_adc_DS_N,
                data_from_adc_DS_P              =>data_from_adc_DS_P,
                Ctrl_reg_in                     =>Ctrl_reg_in,
                Ctrl_reg_out                    =>Ctrl_reg_out,
                x_from_adc_calrun_fmc           =>x_from_adc_calrun_fmc,
                x_from_adc_or_p                 =>x_from_adc_or_p,
                x_from_adc_or_n                 =>x_from_adc_or_n,
                x_to_adc_cal_fmc                =>x_to_adc_cal_fmc,
                x_to_adc_caldly_nscs_fmc        =>x_to_adc_caldly_nscs_fmc,
                x_to_adc_fsr_ece_fmc            =>x_to_adc_fsr_ece_fmc,
                x_to_adc_outv_slck_fmc          =>x_to_adc_outv_slck_fmc,
                x_to_adc_outedge_ddr_sdata_fmc  =>x_to_adc_outedge_ddr_sdata_fmc,
                x_to_adc_dclk_rst_fmc           =>x_to_adc_dclk_rst_fmc,
                x_to_adc_pd_fmc                 =>x_to_adc_pd_fmc,
                x_to_adc_led_0                  =>x_to_adc_led_0,
                x_to_adc_led_1                  =>x_to_adc_led_1
            );
    
        m_axis_tkeep<=not(maxis_cfg(IN_reg_bits-1 downto IN_reg_bits-4)); --to DISABLE |time_msb|time_lsb|d1|d0|
        cmax<=unsigned(maxis_cfg(IN_reg_bits-4 downto 0));
        burst<=cmax when to_integer(cmax) >= aximinburst else
                to_unsigned(aximinburst,IN_reg_bits-3);

        process(m_axis_aclk, m_axis_rstn)
        begin
            if m_axis_rstn='0' then
                counter<=(others=>'0');
            elsif rising_edge(m_axis_aclk) then
                if counter >=burst or m_axis_tready='0' then
                    counter<=(others=>'0');
                else
                    counter<=counter+1;
                end if;
            end if;
        end process;

        m_axis_tlast<='1' when counter=(burst-1) else '0';

end arch_imp;