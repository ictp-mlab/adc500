----------------------------------------------------------------------------------
-- Company: ICTP-MLAB
-- Engineer: Luis G. Garcia
--
-- Create Date: 07/19/2018 04:14:02 PM
-- Design Name: ADC500 controller
-- Module Name: adc500_controller - Behavioral
-- Project Name: ICTP-INFN ADC500 board
-- Target Devices: Zynq, Ultrascale+
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.primitives.all;

entity adc500_controller_nf is
	generic (IN_reg_bits : integer :=32;
	         OUT_reg_bits : integer :=32);
    port (
        --500 Mhz Clock buffer 
        ADC500_CLK_I : IN STD_LOGIC;
        clk_to_adc_DS_P : OUT STD_LOGIC;
        clk_to_adc_DS_N : OUT STD_LOGIC;

        --250 MHz Data Clock Input
        CLK_IN1_D_clk_p : IN STD_LOGIC;
        CLK_IN1_D_clk_n : IN STD_LOGIC;
        DCLK : OUT STD_LOGIC;

        --Data buffer
        data_from_adc_DS_N : IN STD_LOGIC_VECTOR(15 downto 0);
        data_from_adc_DS_P : IN STD_LOGIC_VECTOR(15 downto 0);
        DATA               : OUT STD_LOGIC_VECTOR(15 downto 0);

		Ctrl_reg_in  : in std_logic_vector(IN_reg_bits-1 downto 0);
        Ctrl_reg_out : out std_logic_vector(OUT_reg_bits-1 downto 0);
        
        x_from_adc_calrun_fmc: in std_logic;    --                          
        x_from_adc_or_p : in std_logic_vector(0 downto 0);           -- adc (differential) lvds signal  		
        x_from_adc_or_n : in std_logic_vector(0 downto 0);           -- adc (differential) lvds signal  		
  
        x_to_adc_cal_fmc         : out std_logic ;   --                               
        x_to_adc_caldly_nscs_fmc : out std_logic ;  --                         
        x_to_adc_fsr_ece_fmc     : out std_logic ;  --                                  
        x_to_adc_outv_slck_fmc   : out std_logic ;  --                                
        x_to_adc_outedge_ddr_sdata_fmc : out std_logic ;  --                        
        x_to_adc_dclk_rst_fmc : out std_logic ;   --                                 
        x_to_adc_pd_fmc : out std_logic ;  -- 
        x_to_adc_led_0    : out std_logic ;
        x_to_adc_led_1    : out std_logic
	);
end adc500_controller_nf;
--
architecture arch_imp of adc500_controller_nf is

signal x_from_adc_or : std_logic_vector(0 downto 0);

begin

    --500 MHz output clock buffer.
    OCLK: oclkbuf
        port map(
            clk_in  => ADC500_CLK_I,
            clk_o_p => clk_to_adc_DS_P,
            clk_o_n => clk_to_adc_DS_N
        );

    --250 MHz data clock input
    DataCLK: iclkbuf
    Port map(
        clk_in_p => CLK_IN1_D_clk_p,
        clk_in_n => CLK_IN1_D_clk_n,
        clk_o    => DCLK
    );


    --Input data buffer
    IDATA: idatabuf
    generic map(Nbits => 16)
    port map(
        data_from_adc_DS_N => data_from_adc_DS_N,
        data_from_adc_DS_P => data_from_adc_DS_P,
        DATA => DATA
    );

    --ADC 500 overflow flag
    adc_or: idatabuf 
    generic map(Nbits => 0)
    port map(
        data_from_adc_DS_N => x_from_adc_or_n,
        data_from_adc_DS_P => x_from_adc_or_p,
        DATA => x_from_adc_or
    );



	--	Asynchronous assignement of slv_reg8:
    x_to_adc_cal_fmc <= Ctrl_reg_in(0);   --   
    x_to_adc_caldly_nscs_fmc <= Ctrl_reg_in(1);  -- 
    x_to_adc_fsr_ece_fmc <= Ctrl_reg_in(2);
    x_to_adc_outv_slck_fmc <= Ctrl_reg_in(3);
    x_to_adc_outedge_ddr_sdata_fmc <= Ctrl_reg_in(4) ;
    x_to_adc_dclk_rst_fmc <= Ctrl_reg_in(5) ;
    x_to_adc_pd_fmc <= Ctrl_reg_in(6);
    
    x_to_adc_led_0 <= Ctrl_reg_in(7);
    x_to_adc_led_1 <= Ctrl_reg_in(8);
    
    
    Ctrl_reg_out(0) <= x_from_adc_calrun_fmc;
    Ctrl_reg_out(1) <= x_from_adc_or(0);      -- adc (differential) lvds signal
    Ctrl_reg_out(31 downto 2) <=(others=>'0');

end arch_imp;